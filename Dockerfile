FROM ubuntu:18.04

#ARG model
#ARG domain
#ARG main
ARG pyod

ENV PA /pa_stable_v190600_20161030
ENV HOME /
ENV H /home/pulseaudio
ENV PROJECT /isds
ENV GOOGLE_APPLICATION_CREDENTIALS=cred.json
#ENV MAIN ${main}
#ENV MODEL ${model}
#ENV DOMAIN ${domain}
ENV PYOD ${pyod}

#COPY . /
COPY ./retico ${PROJECT}/retico
COPY ./PyOpenDial ${PROJECT}/PyOpenDial
COPY ./incremental-rasa-nlu ${PROJECT}/incremental-rasa-nlu
COPY ./settings.yml ${PROJECT}
COPY ./cred.json ${PROJECT}
COPY ./start.sh ${PROJECT}

RUN apt-get update
RUN apt-get install -y python3.6 python3-pip libasound-dev wget gcc alsa-base alsa-utils pulseaudio pulseaudio-utils libgl1-mesa-glx

RUN mkdir ${PA}
RUN wget http://www.portaudio.com/archives/pa_stable_v190600_20161030.tgz
RUN tar -xvf pa_stable_v190600_20161030.tgz -C ${PA}
RUN ls ${PA}/portaudio
WORKDIR ${PA}/portaudio
RUN ./configure
RUN make
RUN make install
RUN ldconfig

WORKDIR ${HOME}
RUN pip3 install pyaudio

WORKDIR ${PROJECT}/incremental-rasa-nlu
RUN pip3 install -r requirements.txt
RUN pip3 install -e .

RUN pip3 install google-cloud-speech regex sortedcontainers
RUN pip3 install asteval parse visdom multipledispatch pyqt5
RUN pip3 install pyqt5 soundcard soundfile google-cloud-texttospeech

WORKDIR ${PROJECT}/retico
RUN python3 setup.py install

WORKDIR ${HOME}

COPY ./pulse-conf /etc/pulse
RUN useradd --create-home --home-dir $H pulseaudio \
        && usermod -aG audio,pulse,pulse-access pulseaudio \
        && chown -R pulseaudio:pulseaudio $H

WORKDIR ${PROJECT}

CMD ["/bin/bash", "./start.sh"]

