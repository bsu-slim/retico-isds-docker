# retico-isds-docker

## Description
This is a docker configuration to run a functioning incremental spoken dialogue system built on top of:

* [retico](https://bitbucket.org/bsu-slim/retico/src/master/)
* [Google Speech API](https://cloud.google.com/text-to-speech/docs/quickstart-protocol)
* [Incremental RASA NLU](https://bitbucket.org/bsu-slim/incremental-rasa-nlu/src/master/)
* [PyOpenDial](https://github.com/KAIST-AILab/PyOpenDial)

## Dependencies

* Git
* Docker
* Google Cloud Speech to Text API
* Working microphone

** NOTE: ** only tested on linux (ubuntu 18.04)

## Usage

Follow the steps below in order:

### 1. Get Google Speech to Text API Key

follow the steps [HERE](https://cloud.google.com/text-to-speech/docs/quickstart-protocol).

After you have completed step 4 part f, place the downloaded json file in the project root with the name `cred.json`.

### 2. Build Docker Image

To create the docker image, run `./docker-create-image.sh`

This will create a docker container with the name `isds_image` with the tag `latest`.
you should be able to check if the image created successfully with `docker image ls`.

### 3. Run Docker Container

To run the docker container, run `./docker-run.sh`.

## Running Locally

### 1. Environment Variables

There are several environment variables used by the program to determine where dependencies are located.

* **RASA**: `export RASA="/path/to/rasa/install/"` This is used to specify which rasa build to use and swap them as needed.
* **MODEL**: `export MODEL="/path/to/trained_rasa_nlu/model"` This is used to specify the trained rasa nlu model for rasa to use.
* **DOMAIN**: `export DOMAIN="/path/to/opendial/domain.xml"` This is used to specify the domain to be used by opendial (used as dialogue manager).

### 2. Starting Program

To start the dialogue system, run `python bin/main.py` (make sure you are using python 3.6.x).

## Who Do I Talk To?

Jake Carns (jakecarns@u.boisestate.edu)
Casey Kennington (caseykennington@boisestate.edu)

