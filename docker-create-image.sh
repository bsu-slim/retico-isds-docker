git clone https://github.com/KAIST-AILab/PyOpenDial.git
git clone https://github.com/bsu-slim/rasa_nlu.git
git clone https://bitbucket.org/bsu-slim/retico.git
sudo docker build                 \
  --build-arg pyod="./PyOpenDial" \
  -t isds_image .
