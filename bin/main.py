import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
try:
    from tensorflow.python.util import module_wrapper as deprecation
except ImportError:
    from tensorflow.python.util import deprecation_wrapper as deprecation
deprecation._PER_MODULE_WARNING_LIMIT = 0

from retico.core.audio.io import MicrophoneModule
# from retico.core.audio.io import StreamingSpeakerModule
from retico.modules.google.asr import GoogleASRModule
from retico.modules.rasa.nlu import RasaNLUModule
from retico.modules.opendial.dm import OpenDialModule
from retico.core.debug.general import CallbackModule

# how to restart an utterance?
# had to change is_running to _is_running because opendial modules have an is_running() method

# run: export GOOGLE_APPLICATION_CREDENTIALS=/home/casey/substutute-ca5bdacf1d9a.json

model_dir = os.environ['MODEL']
domain_dir = os.environ['DOMAIN']

def callback_function(iu):
    print(iu)

mic = MicrophoneModule(5000)
asr = GoogleASRModule()
nlu = RasaNLUModule(model_dir=model_dir)
dm = OpenDialModule(domain_dir=domain_dir)
cb = CallbackModule(callback_function)

mic.subscribe(asr)
asr.subscribe(nlu)
asr.subscribe(cb)
nlu.subscribe(dm)
nlu.subscribe(cb)
dm.subscribe(cb)

mic.run()
asr.run()
nlu.run()
dm.run()
cb.run()

input()

mic.stop()
asr.stop()
nlu.stop()
dm.stop()
cb.stop()
