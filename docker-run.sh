sock=$(pactl load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket)
h="${PWD}"
echo $h
snd_opts="
    --device /dev/snd \
    -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
    -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native:Z \
    --group-add $(getent group audio | cut -d: -f3)
"

user_opts="
    -u $(id -u) \
    -e USER=$USER \
    -e USERNAME=$USERNAME \
    -e LOGNAME=$LOGNAME \
"

proj_opts="
    -v ${h}/model:/isds/model \
    -v ${h}/domain:/isds/domain \
    -v ${h}/log:/isds/log \
    -v ${h}/bin:/isds/bin \
    -e RASA=/isds/incremental-rasa-nlu \
    -e MAIN=./bin/main.py \
    -e MODEL=model/nlu_20191117-074519 \
    -e DOMAIN=domain/dialogue.xml \
    -e PROJECT= \
"

dbus_opts="
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /var/run/dbus:/var/run/dbus:Z \
    -v /var/run/dbus:/run/dbus:Z \
    -v /etc/machine-id:/etc/machine-id:ro \
    -e DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS \
    --net=host
"

sudo docker run --rm  \
    --privileged      \
    --name isds       \
    $snd_opt          \
    $proj_opts        \
    $dbus_opts        \
    -ti isds_image ${@}

pactl unload-module ${sock}
